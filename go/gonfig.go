package main

import (
	"gitlab.com/felipemocruha/gonfig"
	"log"
)

type Config struct {
	SomeField string `yaml:"some_field"`
}

func LoadConfig() {
	config := &Config{}

	if err := gonfig.LoadFromEnv(
		"CONFIG_PATH", config, "config.yaml"); err != nil {
		log.Fatalf("Failed to load config: %v", err)
	}
}
