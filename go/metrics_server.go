package main

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func RunMetricsServer(string host) {
	http.Handle("/metrics", promhttp.Handler())
	log.Printf("Starting metrics server at: %v/metrics", METRICS_ADDR)
	log.Println(http.ListenAndServe(host, nil))
}
