package main

import (
	"fmt"
	"log"
	"net/http"
)

type Service struct{}

func (s Service) Handler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	fmt.Fprintf(w, "some data")
}

func RunServer() {
	svc := Service{}

	http.HandleFunc("/", svc.Handler)
	http.HandleFunc("/healthchek",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "ok")
		})

	log.Println(http.ListenAndServe(HOST, nil))
}
