package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Metrics struct {
	errors prometheus.Counter
}

func setupMetrics(m *Metrics) {
	appName := "{{name}}"

	m.errors = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: appName,
		Name:      "errors_total",
		Help:      "Error count.",
	})
}
