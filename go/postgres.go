package main

import (
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = `
CREATE TABLE {{name}} (
  id text PRIMARY KEY,
  name text,
  created_at int
);
`

type dbConfig struct {
	Host     string
	User     string
	Name     string
	Password string
}

func makeConnStr(config *dbConfig) string {
	return fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		config.Host,
		config.User,
		config.Name,
		config.Password,
	)
}

func CreateDB(config *Config) *sqlx.DB {
	db, err := sqlx.Connect("postgres", makeConnStr(config))
	if err != nil {
		log.Fatalf("failed to create database connection: %v", err)
	}
	db.Exec(schema)

	return db
}
