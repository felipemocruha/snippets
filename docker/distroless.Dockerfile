FROM gcr.io/distroless/static

COPY build/{{name}} /

ENTRYPOINT ["/{{name}}"]