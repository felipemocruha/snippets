compile:
	CGO_ENABLED=0 go build -a -ldflags '-w' -o build/{{name}}

benchmark:
	GOGC=off go test -v -bench=. -benchmem

profile:
	GOGC=off go test -bench=. -benchmem -cpuprofile cpu.out -memprofile mem.out

cpu_prof_viz:
	$(MAKE) profile
	go tool pprof -http localhost:8093 --nodefraction=0.1 {{name}}.test cpu.out

mem_prof_viz:
	$(MAKE) profile
	go tool pprof -http localhost:8093 --nodefraction=0.1 {{name}}.test mem.out

allocs:
	go test -v -bench=. -benchmem -gcflags=-m

.PHONY: benchmark
